
package com.mintic.rutas;

import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("serviciosMintic")
public class Routes extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.mintic.servicios.ApiRestEquipos.class);
        resources.add(com.mintic.servicios.ApiRestPartidos.class);
        resources.add(com.mintic.servicios.ApiRestUsuarios.class);
    }
    
}
